const request = require('supertest')
const app = require('../app')

describe("GET /test", () => {
    it('respond with json containing a list of all users', done => {
        request(app)
        .get('/test')
        .set("Accept","application/json")
        .expect("Content-Type", /json/)
        .expect(200, done);
    });
});

describe("GET /test/v1", () => {
    it('respond all endpoints', done => {
        request(app)
        .get('/test/v1')
        .set("Accept","application/json")
        .expect("Content-Type", /json/)
        .expect(200, done);
    });
});

describe("GET /test/location", () => {
    it('responds with city location data', done => {
        request(app)
        .get('/test/location')
        .set("Accept","application/json")
        .expect("Content-Type", /json/)
        .expect(200, done);
    });
});

describe("GET /test/current/:city", () => {
    it('responds with the city weather data', done => {
        request(app)
        .get('/test/current/:city')
        .set("Accept","application/json")
        .expect("Content-Type", /json/)
        .expect(200, done);
    });

    it('responds with the weather data of the current location', done => {
        request(app)
        .get('/test/current')
        .set("Accept","application/json")
        .expect("Content-Type", /json/)
        .expect(200, done);
    });
});

describe("GET /test/forecast/:city", () => {
    it('responds with the city weather data 5 days later', done => {
        request(app)
        .get('/test/forecast/:city')
        .set("Accept","application/json")
        .expect("Content-Type", /json/)
        .expect(200, done);
    });

    it('respond with all endpointsresponds with the weather data of the current location 5 days later', done => {
        request(app)
        .get('/test/forecast')
        .set("Accept","application/json")
        .expect("Content-Type", /json/)
        .expect(200, done);
    });
});