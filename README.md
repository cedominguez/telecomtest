# BackEnd Base

El proyecto fue generado con [ExpressJs](https://expressjs.com/es/starter/generator.html) version 4.16.1.

## Instalación

Ejecutar `npm install` para instalar las dependencias necesarias para el crrecto funcionamiento del servidor.

## Servidor de Desarrollo

Ejecutar `npm start`para levantar un servidor de desarrollo. Consumir API desde `http://localhost:3000/`.
La aplicación de cargara automáticamente al modificar alguno de los archivos de origen.

## Test de Unidad

Ejecutar `npm test` para ejecutar los test unitarios.
