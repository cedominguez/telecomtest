var express = require('express');
var router = express.Router();

/* GET users listing. */
router.get('/', function(req, res, next) {
  return res.json('redirect to /v1');
});

router.get('/v1', function(req, res, next) {
  return res.json('respond with all endpoints');
});

router.get('/location', function(req, res) {
  return res.json('responds with city location data');
});

router.get('/current/:city?', function(req, res) {
  if (req.params.city) {
    return res.json('responds with the city weather data');
  }
  return res.json('responds with the weather data of the current location')
});

router.get('/forecast/:city?', function(req, res) {
  if (req.params.city) {
    return res.json('responds with the city weather data 5 days later');
  }
  return res.json('respond with all endpointsresponds with the weather data of the current location 5 days later');
});

module.exports = router;
