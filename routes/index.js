var express = require('express');
var router = express.Router();
var ipapi = require('ipapi.co');

const https = require('https');

// Función utilizada para retorno de API de ip-api
var callback = function(res, city, forecast=false){
  const location = city;
  const appId = "8855be42c374114679ba0999cb41c7e0";
  var url = forecast ? "https://api.openweathermap.org/data/2.5/forecast?q=":"https://api.openweathermap.org/data/2.5/weather?q=";
  url += location + "&appid=" + appId;
  console.log(url);
  https.get(url, (response) => {
    if (response.statusCode === 200) {
      response.on("data", (data) => {
        const weatherData = JSON.parse(data);
        res.status(200).json({data: weatherData});
      });
    } else {
      res.status(400).json({data: "0"});
    }
  })
};

var callbackForecast = function(res,city){
  callback(res, city, true);
};

// Rutas para consumir API

router.get('/', function(req, res) {
  res.redirect('/v1');
});

router.get('/v1', function(req, res, next) {
  res.status(200).json({
    "/location": "Devuelve los datos de ubicación city según ip-api",
    "/current[/city]": "City es un parámetro opcional. Devuelve los datos de ubicación city o la ubicación actual según " +
    "ip-api y el estado del tiempo actual.",
    "/forecast[/city]": "City es un parámetro opcional. Devuelve los datos de ubicación city o la ubicación actual según" +
    "ip-api y el estado del tiempo a 5 días"
	});
});

router.get('/location', function(req, res) {
  ipapi.location(callback.bind(null, res),'','','city');
});

router.get('/current/:city?', function(req, res) {
  const city = req.params.city;
  if (city) {
    callback(res, city);
  }else{
    ipapi.location(callback.bind(null, res),'','','city');
  }
});

router.get('/forecast/:city?', function(req, res) {
  const city = req.params.city;
  if (city) {
    callbackForecast(res, city);
  }else{
    ipapi.location(callbackForecast.bind(null, res),'','','city');
  }
});

module.exports = router;
